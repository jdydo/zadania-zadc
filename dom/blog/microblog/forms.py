# -*- coding=utf-8 -*-
import models
import djangoforms


class PostForm(djangoforms.ModelForm):
    class Meta:
        model = models.Posts
        exclude = ['author', 'date']