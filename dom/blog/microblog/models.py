from google.appengine.ext import db


class Posts(db.Model):
    author = db.UserProperty(auto_current_user_add=True)
    title = db.StringProperty(verbose_name='Tytul')
    text = db.TextProperty(verbose_name='Tresc')
    date = db.DateTimeProperty(auto_now_add=True)