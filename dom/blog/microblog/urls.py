from django.conf.urls import patterns, url
from views import AllPosts, AddPost, Login, Logout, Worker


urlpatterns = patterns('',
    url(r'^$', AllPosts.as_view(), name='all_posts'),
    url(r'^add/$', AddPost.as_view(), name='add_post'),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^worker/$', Worker.as_view(), name='worker'),
)