# -*- coding=utf-8 -*-
from time import sleep
from django.views.generic import View
from django.shortcuts import render, redirect
from django.utils import timezone
from models import *
from forms import *
import sortowanie
from google.appengine.api import users
from google.appengine.api import memcache
import requests
import logging
from google.appengine.ext import deferred

# python appcfg.py update "/home/jakub/Pulpit/GIT ZadC/dom/blog/"


class Worker(View):
    def get(self, request):
        req = requests.get("http://www.random.org/integers/?num=10&min=0&max=20&col=1&base=10&format=plain&rnd=new")
        result = memcache.get('p1_%s' % str(req.content).replace('\n', ' '))

        if result is None:
            result = deferred.defer(sortowanie.sort, req)
            i = 0
            while True:
                result = memcache.get('p1_%s' % str(req.content).replace('\n', ' '))
                if result is not None:
                    break
                else:
                    i += 1
            logging.info('Praca zakonczona - czas: %s jednostek' % str(i))
            logging.info('Rekord dodany: %s' % 'p1_%s' % str(req.content).replace('\n', ' '))
        else:
            logging.info('Rekord znaleziony: %s' % 'p1_%s' % str(req.content).replace('\n', ' '))

        return render(request, 'microblog/worker.html', {'list1': str(req.content).replace('\n', ', ').rstrip(', '),
                                                         'list2': str(result).rstrip(']').lstrip('['),
                                                         'g_user': users.get_current_user()})


class AllPosts(View):
    def get(self, request):
        return render(request, 'microblog/show_posts.html', {'posts': Posts.all().order('-date').fetch(20),
                                                             'g_user': users.get_current_user()})


class AddPost(View):
    def get(self, request, *args, **kwargs):
        if users.get_current_user():
            return render(request, 'microblog/add_post.html', {'form': PostForm(),
                                                               'g_user': users.get_current_user()})
        else:
            return redirect(users.create_login_url('/add'))

    def post(self, request, *args, **kwargs):
        if users.get_current_user():
            add_form = PostForm(request.POST)
            if add_form.is_valid():
                add_form.save()
                return render(request, 'microblog/ok.html', {'text': 'Wpis dodany!',
                                                             'g_user': users.get_current_user()})
            else:
                return render(request, 'microblog/add_post.html', {'form': add_form,
                                                                   'g_user': users.get_current_user()})
        else:
            return redirect(users.create_login_url('/add'))


class Login(View):
    def get(self, request):
        return redirect(users.create_login_url('/'))


class Logout(View):
    def get(self, request, *args, **kwargs):
        return redirect(users.create_logout_url('/'))