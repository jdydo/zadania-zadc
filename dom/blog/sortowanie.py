from google.appengine.api import memcache


def sort(req):
    splited = req.content.split('\n')
    result = list()
    for number in splited:
        try:
            result.append(int(number))
        except ValueError:
            pass
    result.sort()
    memcache.add('p1_%s' % str(req.content).replace('\n', ' '), result, 3600)